import React from 'react';
import { ConnectedRouter } from 'connected-react-router';
import { Switch } from 'react-router-dom';
import history from './lib/history';
import Routes from './routes';

export default function App() {
  return (
    <ConnectedRouter history={history}>
      <Switch children={Routes} />
    </ConnectedRouter>

  );
}