import { createBrowserHistory } from 'history'
import qhistory from 'qhistory';
import { stringify, parse } from 'qs';

const history = qhistory(
    createBrowserHistory({
        basename: 'foobar'
    }),
    stringify,
    parse
);

export default history;