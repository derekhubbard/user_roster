import UsersAPI from '../../core/Users/UsersAPI';

export const FETCH_USERS = 'FETCH_USERS';
function fetchUsers(sortBy) {
    return dispatch => {
        UsersAPI.fetchUsers(sortBy)
            .then(
                r => r.json(), 
                e => console.log(e)
            ).then(
                users => dispatch(fetchUsersComplete(users))
            );
    }
}

export const FETCH_USERS_COMPLETE = 'FETCH_USERS_COMPLETE';
function fetchUsersComplete(users) {
    return {
        type: FETCH_USERS_COMPLETE,
        users
    };
}

export const ADD_USER = 'ADD_USER';
function addUser(user) {
    return dispatch => {
        UsersAPI.addUser(user)
            .then(
                r => r.json(),
                e => console.log(e)
            ).then(
                r => dispatch(fetchUsers())
            );
    }
}

export const DELETE_USER = 'DELETE_USER';
function deleteUser(id, sortBy) {
    return dispatch => {
        UsersAPI.deleteUser(id)
            .then(
                r => dispatch(fetchUsers(sortBy)),
                e => console.log(e)
            );
    }
}

export default {
    fetchUsers,
    fetchUsersComplete,
    addUser,
    deleteUser
};