import React, { Component } from 'react';
import { connect } from 'react-redux';
import UsersListComponent from './UsersListComponent';
import UsersActions from '../UsersActions';

const mapStateToProps = ({ users }) => {
    return {
        users
    };
}

class UsersListContainer extends Component {
    constructor(props) {
        super(props);
        this.sortUsers = this.sortUsers.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(UsersActions.fetchUsers('name'));
    }

    sortUsers(sortBy) {
        const { dispatch } = this.props;
        dispatch(UsersActions.fetchUsers(sortBy))
    }

    deleteUser(id, sortBy) {
        const { dispatch } = this.props;
        dispatch(UsersActions.deleteUser(id, sortBy));
    }

    render() {
        return <UsersListComponent {...this.props} sortUsers={this.sortUsers} deleteUser={this.deleteUser} />;
    }
}

export default connect(mapStateToProps)(UsersListContainer);