import React, { Component } from 'react';
import { connect } from 'react-redux';
import NewUserFormComponent from './NewUserFormComponent';
import UsersActions from '../UsersActions';

class NewUserFormContainer extends Component {
    constructor(props) {
        super(props);
        this.addUser = this.addUser.bind(this);
    }

    addUser(user) {
        const { dispatch } = this.props;
        dispatch(UsersActions.addUser(user))
    }

    render() {
        return <NewUserFormComponent {...this.props} addUser={this.addUser}/>;
    }
}

export default connect()(NewUserFormContainer);