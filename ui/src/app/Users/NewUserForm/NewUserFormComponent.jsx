import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles(theme => ({
    root: {
      display: 'flex',
      justifyContent: 'flex-end'
    },
    addButton: {
      margin: theme.spacing(1),
    },
  }));

export default function NewUserFormComponent({ addUser }) {
    const classes = useStyles();

    const [open, setOpen] = useState(false);
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const [birthday, setBirthday] = useState();
    const [zipcode, setZipcode] = useState();

    function handleClickOpen() {
      setOpen(true);
    }
  
    function handleClose() {
      setOpen(false);
    }

    function handleSubmit(e) {
        e.preventDefault();
        addUser({
            name,
            email,
            birthday,
            zipcode
        });
        handleClose();
    }

    return (
        <div className={classes.root}>
            <Fab color="primary" aria-label="Add" className={classes.addButton} onClick={handleClickOpen}>
                <AddIcon />
            </Fab>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Add User</DialogTitle>
                <form>
                    <DialogContent>
                        <TextField
                            autoFocus
                            id="name"
                            label="Name"
                            fullWidth
                            onChange={e => setName(e.target.value)}
                        />                    
                        <TextField
                            margin="dense"
                            id="email"
                            label="Email"
                            type="email"
                            fullWidth
                            onChange={e => setEmail(e.target.value)}
                        />
                        <TextField
                            margin="dense"
                            id="birthday"
                            label="Birthday"
                            type="date"
                            fullWidth
                            InputLabelProps={{
                                shrink: true
                            }}    
                            onChange={e => setBirthday(e.target.value)}                    
                        />
                        <TextField
                            margin="dense"
                            id="zipcode"
                            label="ZIP Code"
                            fullWidth
                            onChange={e => setZipcode(e.target.value)}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button type="submit" onClick={handleSubmit} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </form>
            </Dialog>
        </div>
    );
};