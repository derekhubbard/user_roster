import fetch from 'cross-fetch';

const headers = {
    'Content-Type': 'application/json',
}

function fetchUsers(sortByField) {
    return fetch(`/api/v1/users?sortBy=${sortByField}`);
}

function addUser(user) {
    return fetch('/api/v1/users', { 
        method: 'POST',
        body: JSON.stringify(user),
        headers
    });
}

function deleteUser(id) {
    return fetch(`/api/v1/users/${id}`, {
        method: 'DELETE'
    })
}

export default {
    fetchUsers,
    addUser,
    deleteUser
};