export const safely = async func => {
    try {
        return await func();
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
};