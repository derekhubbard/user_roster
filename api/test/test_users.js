import _ from 'lodash';
import chai from 'chai';
import config from 'config';
import App from 'app';
import { safely } from './utils';
import request from 'supertest';
import { Z_OK } from 'zlib';

const expect = chai.expect;

describe('users api', () => {
    let app, server;

    before(async () => {
        app = await safely(App.start)
        server = app.server;
    });

    after(async () => {
        if (app) {
            await safely(app.shutdown);
        }
    });

    describe('find all users', () => {
        it('should return list of users', async () => {
            const { body: results } = await request(server)
                .get(`${config.prefix}/users`)
                .expect(200);

            expect(results).to.be.an('array').with.length(5);
            results.forEach(result => {
                expect(result).to.have.property('name');
                expect(result).to.have.property('email');
                expect(result).to.have.property('birthday');
                expect(result).to.have.property('zipcode');
            });
        });

        it('should support sorting users by name', async () => {
            const sortByField = 'name';
            const { body: results } = await request(server)
                .get(`${config.prefix}/users?sortBy=${sortByField}`)
                .expect(200);

            expect(results).to.be.an('array').with.length(5);
            const sortedUsers = _.sortBy(results, sortByField);
            expect(results).to.eql(sortedUsers);
        });

        it('should support sorting users by birthday', async () => {
            const sortByField = 'birthday';
            const { body: results } = await request(server)
                .get(`${config.prefix}/users?sortBy=${sortByField}`)
                .expect(200);

            expect(results).to.be.an('array').with.length(5);
            const sortedUsers = _.sortBy(results, sortByField);
            expect(results).to.eql(sortedUsers);            
        });

        it('should support sorting users by email', async () => {
            const sortByField = 'email';
            const { body: results } = await request(server)
                .get(`${config.prefix}/users?sortBy=${sortByField}`)
                .expect(200);

            expect(results).to.be.an('array').with.length(5);
            const sortedUsers = _.sortBy(results, sortByField);
            expect(results).to.eql(sortedUsers);            
        });   

        it('should support sorting users by zipcode', async () => {
            const sortByField = 'zipcode';
            const { body: results } = await request(server)
                .get(`${config.prefix}/users?sortBy=${sortByField}`)
                .expect(200);

            expect(results).to.be.an('array').with.length(5);
            const sortedUsers = _.sortBy(results, sortByField);
            expect(results).to.eql(sortedUsers);            
        });
    });

    describe('find user by id', () => {
        it('should support retrieving user by id', async () => {
            const { body: users } = await request(server)
                .get(`${config.prefix}/users`)
                .expect(200);

            const [user, ...rest] = users;
            const { body: result } = await request(server)
                .get(`${config.prefix}/users/${user.id}`)
                .expect(200);

            expect(result).to.eql(user);
        });
    })

    describe('add new user', () => {
        it('should support adding a new user', async () => {
            const newUser = {
                name: 'Thanos',
                email: 'thanos@notanavenger.com',
                birthday: new Date(1900, 1, 15),
                zipcode: '12345'
            };

            const { body: result } = await request(server)
                .post(`${config.prefix}/users`)
                .send(newUser)
                .expect(200);
            expect(result).to.have.property('id');
            expect(result).to.have.property('name', newUser.name);

            const { body: users } = await request(server)
                .get(`${config.prefix}/users`)
                .expect(200);
            expect(users).to.be.an('array').with.length(6);
            const userNames = _.map(users, 'name');
            expect(userNames).to.contain(newUser.name)
        });
    });

    describe('update existing user', () => {
        it('should support updating an existing user by id', async () => {
            const { body: users } = await request(server)
                .get(`${config.prefix}/users`)
                .expect(200);
            const [user, ...rest] = users;

            const updatedUser = {
                ...user,
                name: 'Renamed Avenger',
                birthday: new Date(2000, 0, 1).toString(),
                email: 'new_email@avengers.com',
                zipcode: '12345'
            };
            const { body: updateResult } = await request(server)
                .put(`${config.prefix}/users`)
                .send(updatedUser)
                .expect(200);

            expect(updateResult).to.eql(updatedUser);
        });
    });

    describe('delete existing user', () => {
        it('should support removing existing user by id', async () => {
            const { body: users } = await request(server)
                .get(`${config.prefix}/users`)
                .expect(200);
            const [user, ...rest] = users;

            const result = await request(server)
                .del(`${config.prefix}/users/${user.id}`)
                .expect(204);

            const { body: usersPostDelete } = await request(server)
                .get(`${config.prefix}/users`)
                .expect(200);
            expect(users).to.have.length(usersPostDelete.length + 1);
        });
    })
})