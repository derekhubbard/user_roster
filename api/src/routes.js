import config from 'config';
import Router from 'koa-router';
import usersController from './controllers/users';

const router = new Router({
    prefix: config.prefix
});

router.get('/users', usersController.findAll);
router.post('/users', usersController.add);
router.put('/users', usersController.update);
router.get('/users/:id', usersController.findById);
router.del('/users/:id', usersController.removeById);

export default router.routes();