import App from './app';
import config from 'config';

App.start()
    .then(() => {
        console.log('serving on', config.port);
    })
    .catch(e => {
        console.error('Startup Error:', e);
        process.exit(1);
    });