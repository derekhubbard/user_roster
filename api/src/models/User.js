import _ from 'lodash';

const User = (function () {
    let db, lastId = 0;

    function createRecord(name, email, birthday, zipcode) {
        return { 
            id: ++lastId, 
            name, 
            email, 
            birthday, 
            zipcode 
        };
    }    
 
    function createDb() {
        console.log('Initializing database...');
        return [
            createRecord('Captain Marvel', 'captain.marvel@avengers.com', new Date('1911-09-16'), '80241'),
            createRecord('Thor', 'thor@avengers.com', new Date('1936-07-22'), '30269'),
            createRecord('Iron Man', 'iron.man@avengers.com', new Date('1944-05-31'), '01085'),
            createRecord('Black Widow', 'black.widow@avengers.com', new Date('1993-04-03'), '74820'),  
            createRecord('Spider-Man', 'spider.man@avengers.com', new Date('2010-02-13'), '40207'),            
        ]
    }

    function createDbIfNotDefined() {
        if (!db) {
            db = createDb();
        }
    }
 
    return {
        find: id => {
            createDbIfNotDefined();

            if (id) {
                return _.find(db, { id: Number(id)})
            }
            
            return db;
        },
        add: user => {
            createDbIfNotDefined();

            user.id = ++lastId;
            user.birthday = new Date(user.birthday);
            db.push(user);
            return user;
        },
        update: user => {
            createDbIfNotDefined();

            const userId = Number(user.id);
            const existingUserIndex = _.findIndex(db, { id: userId });
            db.splice(existingUserIndex, 1, user);
            return _.find(db, { id: Number(userId) });
        },
        remove: id => {
            createDbIfNotDefined();

            const userId = Number(id);
            _.remove(db, user => user.id === userId);
        }
    };
})();

export default User;