# User Roster

_User Roster_ is a minimalistic and simple user management application. If you struggle to keep track of all of your application's users, then tracking them with _User Roster_ may be the solution for you!

## Features

_User Roster_ includes the following features: 
+ Viewing a list of all users, including a user's name, email, birthdate and zip code. 
+ Sorting users by all user fields.
+ Adding new users.
+ Deleting users.

## Getting Started

To download, build and run _User Roster_ on your computer follow these steps:

1. Clone the _User Roster_ repository:

    `git clone git@gitlab.com:derekhubbard/user_roster.git`

2. Build and run the application using Docker:

    `docker-compose up --build`

3. Open your browser to the following url:

    `http://localhost:3000/`

That's it!

## System Requirements and Dependencies

To bulid and run _User Roster_ on your computer using Docker, you will first need to install the Docker application. 

All other application dependencies will be automatically installed when running `docker-compose` and starting the application (see "Getting Started" section above).

More information on Docker is available at the following urls: 

+ Docker: https://www.docker.com/
+ Install Docker Desktop for Mac: https://docs.docker.com/docker-for-mac/install/

## Project Structure

_User Roster_ is composed of two applications: a backend server and a frontend web application. 

The source code for both applications is contained in the _User Roster_ repository and is separated by folders at the root of the repository with the name of each project: `api` for the backend server, and `ui` for the frontend web application.

### UI

The `ui` project is a single page application built with React, Redux, Redux Thunk, and Material-UI. 

The intial project structure and configuration were created using Create React App - a simple library for creating and bootstrapping new React applications. 

User interface components theming is provided by Material-UI, which is collection of React components that implement Google's Material Design specification.

State management is managed with Redux and Redux Thunk.

React components within the `ui` project are organized by functional area, which will allow the project structure to easily scale as the amount of application code grows.

Also, as the `ui` project evolves, we will consider "ejecting" the initial configuration structure provided by Create React App. This will give us more control over the environment, dependencies, and configuration in the future.

Additional information on React, Redux, Redux Thunk, and Create React App can be found at the urls below:

+ React: https://reactjs.org/
+ Redux: https://redux.js.org/
+ Redux Thunk: https://github.com/reduxjs/redux-thunk
+ Create React App: https://facebook.github.io/create-react-app/
+ Material-UI: https://material-ui.com/
+ Material Design: https://material.io/

### API

The `api` project is a node application built with the Koa web framework.

The application startup code uses a simple promise-based pattern for starting and initializing any application dependencies like databases, email services, etc. 

The `api` includes integration tests for the apis to ensure each works as expected.

Tests are executed using Mocha and assertions are provided by Chai. 

The following commands can be used to run the tests: 

+ run all tests: `yarn test` 
+ run all tests (in watch mode): `yarn test:watch`

An in-memory data store (created as a Singleton using an IIFE block) is included for persisting the "users" in memory.

Additional information on Koa and other libraries used in the `api` project is included below: 

+ Koa: https://koajs.com/
+ Mocha: https://mochajs.org/
+ Chai: https://www.chaijs.com/

## Future Enhancements

Future enhancements include: 

+ Redesigning the theme of the user interface. Material UI can be extended through the use of themes, allowing support for more customized and unique branding and styles.

+ Adding support for editing existing users. This can be accomplished by extracting the existing "New User" form into a shared component and then using it for creating new users and editing existing users.

+ Adding support for sorting users in descending order by each user field (name, email, birthday, zipcode, etc).

+ Introducing a more sophisticated storage solution, possibly either a relational database like Postgres or a document database like MongDB. 

+ Adding the ability to search and/or filter users, using an open source search platform such as Elasticsearch.

+ Adding paging support to the api for retrieving all users. Most search platforms (such as Elasticsearch) include the ability to paginate results, so paging support could be added at the same time as search/filter support.

+ Adding sample data, or possibly a mock api, to the `ui` application, which would allow the two applications to be decoupled from each other and developed independently. 

+ Build and deployment pipelines (Gitlab CI, Jenkins/TeamCity, AWS, etc).